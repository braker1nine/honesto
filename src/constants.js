export const QuestionType = {
    RATING: 'RATING',
    TEXT: 'TEXT',
    MULTIPLECHOICE: 'MULTIPLECHOICE'
}
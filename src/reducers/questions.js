/*
    Question 
    {
        type: QuestionType
        text: String
        options: [String] (optional)
        answer: Any
    }
*/
import { QuestionType } from '../constants'

const initialState = {
    1: {
        id: 1,
        type: QuestionType.MULTIPLECHOICE,
        text: "How well does this person participate?",
        options: [
            "Please Improve<br>You may have done one or the following: Maybe you were mostly quiet in meetings and when you had something on your mind, you brought it to the team afterward. Or, you had feedback that would be valuable to go, but you found it too difficult. Or, you had an opportunity to grow by doing something uncomfortable but you didn’t.", 
            "You Were Good<br>You sometimes participate in meetings but you feel that they don’t always bring up important things when they should."
        ],
    },
    2: {
        id: 2,
        type: QuestionType.TEXT,
        text: "How does this person understand the thing?",
    },
    3: {
        id: 3,
        type: QuestionType.RATING,
        text: "How would you rate your performance with X?",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },
    4: {
        id: 4,
        type: QuestionType.RATING,
        text: "How is working with this person?",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },
    5: {
        id: 5,
        type: QuestionType.TEXT,
        text: "Describe a time this person overcame adversity?",
    },
    6: {
        id: 6,
        type: QuestionType.MULTIPLECHOICE,
        text: "This person handles working autonomously well",
        options: [
            "Strongly Agree",
            "Agree", 
            "Disagree",
            "Strongly Disagree"
        ],
    },
};

function questions(state = initialState, action) {
    return state
}

export default questions;
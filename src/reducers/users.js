const initialState = {
    1: {
        name: "Jane Smith",
        avatar: "/images/47.jpg",
        id: 1
    },
    2: {
        name: "Chris Johnson",
        avatar: "/images/22.jpg",
        id: 2
    },
    3: {
        name: "Nico Perez",
        avatar: "/images/43.jpg",
        id: 3
    },
    4: {
        name: "Nathaniel Moon",
        avatar: "/images/46.jpg",
        id: 4
    },
    5: {
        name: "Sally Denison",
        avatar: "/images/65.jpg",
        id: 5
    },
    6: {
        name: "Ruth Carter",
        avatar: "/images/68.jpg",
        id: 6
    }   
};

function users(state = initialState, action) {
    return state
}

export default users;
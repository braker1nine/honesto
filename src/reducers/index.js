import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import questionnaires from './questionnaires';
import users from './users';
import app from './app';
import questions from './questions';

export default (history) => combineReducers({
    router: connectRouter(history),
    questionnaires,
    users,
    app,
    questions,
})
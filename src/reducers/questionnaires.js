import {
    QUESTION_ANSWER,
    QUESTIONNAIRE_COMPLETE
} from '../actions'

/*
    Question 
    {
        type: QuestionType
        text: String
        options: [String] (optional)
        answer: Any
    }

    Questionnaire
    {
        questions: [Question]
        reviewer: Number (user id)
        reviewee: Number (user id)
        id: String
        complete: Boolean
    }
*/

const initialState = {
    1: {
        questions: [
            { questionId: 1, answer: null },
            { questionId: 2, answer: null },
            { questionId: 3, answer: null },
            { questionId: 4, answer: null }
        ],
        id: 1,
        reviewer: 1,
        reviewee: 3,
        complete: false,
    },
    2: {
        questions: [
            { questionId: 1, answer: 1 },
            { questionId: 2, answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Risus sed vulputate odio ut enim blandit volutpat maecenas. Id ornare arcu odio ut sem nulla pharetra diam sit. Odio facilisis mauris sit amet massa vitae. Tellus elementum sagittis vitae et leo. Feugiat sed lectus vestibulum mattis ullamcorper velit. Nisl purus in mollis nunc sed id. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Faucibus turpis in eu mi bibendum. Sit amet tellus cras adipiscing enim eu. Vitae tempus quam pellentesque nec nam aliquam sem et tortor." },
            { questionId: 3, answer: 4 },
            { questionId: 4, answer: 7 },
            { questionId: 5, answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nullam eget felis eget nunc lobortis mattis aliquam faucibus. Lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit. Eleifend mi in nulla posuere sollicitudin aliquam ultrices. Congue nisi vitae suscipit tellus mauris a diam maecenas. Sit amet aliquam id diam maecenas ultricies mi eget mauris. Vulputate odio ut enim blandit. Nibh sed pulvinar proin gravida. Luctus accumsan tortor posuere ac ut consequat semper viverra. Etiam non quam lacus suspendisse."},
            {questionId: 6, answer: 3 },
        ],
        id: 2,
        reviewer: 1,
        reviewee: 5,
        complete: true,
    },
    3: {
        questions: [
            { questionId: 1, answer: 0 },
            { questionId: 2, answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Risus sed vulputate odio ut enim blandit volutpat maecenas. Id ornare arcu odio ut sem nulla pharetra diam sit. Odio facilisis mauris sit amet massa vitae. Tellus elementum sagittis vitae et leo. Feugiat sed lectus vestibulum mattis ullamcorper velit. Nisl purus in mollis nunc sed id. Nulla aliquet porttitor lacus luctus accumsan tortor posuere. Faucibus turpis in eu mi bibendum. Sit amet tellus cras adipiscing enim eu. Vitae tempus quam pellentesque nec nam aliquam sem et tortor." },
            { questionId: 3, answer: 5 },
            { questionId: 4, answer: 2 },
            { questionId: 5, answer: null},
            {questionId: 6, answer: 1 },
        ],
        id: 4,
        reviewer: 2,
        reviewee: 1,
        complete: true,
    },
    4: {
        questions: [
            { questionId: 1, answer: null },
            { questionId: 2, answer: "Risus sed vulputate odio ut enim blandit volutpat maecenas. Id ornare arcu odio ut sem nulla pharetra diam sit. Odio facilisis mauris sit amet massa vitae. Tellus elementum sagittis vitae et leo. Faucibus turpis in eu mi bibendum. Sit amet tellus cras adipiscing enim eu. Vitae tempus quam pellentesque nec nam aliquam sem et tortor." },
            { questionId: 3, answer: 6 },
            { questionId: 4, answer: 9 },
            { questionId: 5, answer: "Nullam eget felis eget nunc lobortis mattis aliquam faucibus. Lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit. Eleifend mi in nulla posuere sollicitudin aliquam ultrices. Congue nisi vitae suscipit tellus mauris a diam maecenas. Sit amet aliquam id diam maecenas ultricies mi eget mauris. Vulputate odio ut enim blandit. Nibh sed pulvinar proin gravida. Luctus accumsan tortor posuere ac ut consequat semper viverra. Etiam non quam lacus suspendisse."},
            {questionId: 6, answer: 2 },
        ],
        id: 4,
        reviewer: 1,
        reviewee: 4,
        complete: true,
    },
    5: {
        questions: [
            { questionId: 1, answer: 1 },
            { questionId: 2, answer: "Risus sed vulputate odio ut enim blandit volutpat maecenas. Id ornare arcu odio ut sem nulla pharetra diam sit. Odio facilisis mauris sit amet massa vitae. Tellus elementum sagittis vitae et leo. Faucibus turpis in eu mi bibendum. Sit amet tellus cras adipiscing enim eu. Vitae tempus quam pellentesque nec nam aliquam sem et tortor." },
            { questionId: 3, answer: 6 },
            { questionId: 4, answer: null },
            { questionId: 5, answer: "Nullam eget felis eget nunc lobortis mattis aliquam faucibus. Lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit. Eleifend mi in nulla posuere sollicitudin aliquam ultrices. Congue nisi vitae suscipit tellus mauris a diam maecenas. Sit amet aliquam id diam maecenas ultricies mi eget mauris. Vulputate odio ut enim blandit. Nibh sed pulvinar proin gravida. Luctus accumsan tortor posuere ac ut consequat semper viverra. Etiam non quam lacus suspendisse."},
            {questionId: 6, answer: 2 },
        ],
        id: 5,
        reviewer: 3,
        reviewee: 1,
        complete: true,
    },
    6: {
        questions: [
            { questionId: 1, answer: null },
            { questionId: 2, answer: null },
            { questionId: 3, answer: null },
            { questionId: 6, answer: null }
        ],
        id: 6,
        reviewer: 1,
        reviewee: 6,
        complete: false,
    },
};

function questionnaires(state = initialState, action) {
    switch(action.type) {
        case QUESTION_ANSWER:
            return {
                ...state,
                [action.questionnaireId]: {
                    ...state[action.questionnaireId],
                    questions: state[action.questionnaireId].questions.map((question, index) => {
                        if (index !== action.questionIndex) {
                            return question
                        }

                        return {
                            ...question,
                            answer: action.answer
                        }
                    })
                }
            }
        case QUESTIONNAIRE_COMPLETE:
            return {
                ...state,
                [action.questionnaireId]: {
                    ...state[action.questionnaireId],
                    complete: true,
                }
            }
        default:
            return state;
    }
}

export default questionnaires;
import React, { Component } from 'react';
import classnames from 'classnames';

class Rating extends Component {

    onClick(e) {
        if (this.props.setValue) {
            let rating = parseInt(e.target.dataset.rating);
            this.props.setValue(rating)
        }
    }

    render() {
        let ratingItems = []
        for (var i = 0; i < 10; i++) {
            ratingItems.push(
                <span 
                    key={i}
                    data-rating={i+1}
                    className={classnames("rating-item", { active: i < this.props.value})}
                    onClick={this.onClick.bind(this)}
                />
            )
        }
        
        let style;
        if (this.props.height) {
            style = { height: `${this.props.height}px`}
        }

        return (
            <div className="rating" style={this.props.style}>
                <div className={classnames("rating-items", { "read-only" : !this.props.setValue})} style={style}>
                    {ratingItems}
                </div>
            </div>
        )
    }
}

export default Rating;
import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Avatar } from 'antd';

const Header = ({ user }) => {
    return (
        <header className="app-header">
            <h1><Link to="/share">honesto</Link></h1>
            <nav className="app-header__nav">
                <NavLink className="app-header__nav__item" to="/share">Share Feedback</NavLink>
                <NavLink className="app-header__nav__item" to="/my">My Feedback</NavLink>
                <NavLink className="app-header__nav__item" to="/team">Team Feedback</NavLink>
            </nav>
            <div className="user-details" style={{display: 'flex', alignItems: 'center', height: '72px', borderLeft: '1px solid #D9DCDE', marginLeft: 'auto', minWidth: '240px'}}>
                <Avatar src={user.avatar} size={58} style={{margin: '0 16px'}} />
                <div>
                    <div>{user.name}</div>
                    <Link to="/login" className="uppercase-text">Logout</Link>
                </div>
            </div>
        </header>
    );
}

const mapStateToProps = (state, props) => {

    return {
        user: state.users[state.app.currentUser]
    }
}

export default connect(mapStateToProps)(Header);
import React, { Component } from 'react';
import { Input } from 'antd';

const { TextArea } = Input;

class Text extends Component {

    constructor(props) {
        super(props)
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        const text = e.target.value;
        this.props.setAnswer(text);
    }

    render() {
        return (
            <div className="multiple-choice">
                <TextArea placeholder="Say something...." value={this.props.answer} autosize={{minRows: 8, maxRows: 8}} onChange={this.onChange}/>
            </div>
        )
    }
}

export default Text;
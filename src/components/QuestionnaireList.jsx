import React, { Component } from 'react';
import { List, Avatar, Typography, Button } from 'antd';
import { Link } from 'react-router-dom';

class QuestionnaireList extends Component {

    renderItem(item) {
        return(
            <List.Item className="hoverable">
                <Avatar src={item.reviewee.avatar} size={58} style={{marginRight: "28px"}}/>
                <Typography.Text style={{fontSize: '22px'}}>{item.reviewee.name}</Typography.Text>
                <Link style={{marginLeft: "auto"}} to={`/${item.complete ? 'team' : 'share'}/${item.id}`}>
                    <Button style={{width:"150px"}} type={item.complete ? null : "primary"} size="large">
                        {item.complete ? "View Submission" : "Fill Out"}
                    </Button>
                </Link>
            </List.Item>
        );
    }

    render() {
        return (
            <List 
                bordered
                dataSource={this.props.questionnaires}
                renderItem={this.renderItem}
                locale={{ emptyText: (
                    <div>
                        <img width="140" src="/images/bee.svg" alt="Empty Smiley"/>
                        <p style={{fontWeight: 'bold', marginTop: '8px', marginBottom: 0}}>Way to bee!</p>
                        <p style={{maxWidth: "300px", margin: 'auto'}}>No more reviews for you to give! <Link to="/my">Go see</Link> what you coworkers are saying about you.</p>
                    </div>
                    )
                }}
            />
        )
    }
}

export default QuestionnaireList;
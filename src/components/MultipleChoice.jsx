import React, { Component } from 'react';
import classnames from 'classnames';

class MultipleChoice extends Component {
    render() {
        
        return (
            <div className="multiple-choice">
                {this.props.options.map((option, index) => 
                    <div 
                        key={index}
                        className={classnames("multiple-choice__answer", {active: index === this.props.answer})}
                        onClick={() => this.props.setAnswer(index)}
                    >
                        <span dangerouslySetInnerHTML={{__html: option}} />
                    </div>)}
            </div>
        )
    }
}

export default MultipleChoice;
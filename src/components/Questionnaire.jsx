import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Icon, Avatar, Typography, Card, Button, Progress } from 'antd'
import { Link } from 'react-router-dom';

import { QuestionType } from '../constants';
import { mapQuestionnaire, questionnairesForUser } from '../selectors';
import MultipleChoice from './MultipleChoice';
import Text from './Text';
import Rating from './Rating';
import QuestionnaireList from './QuestionnaireList';
import { QUESTION_ANSWER, QUESTIONNAIRE_COMPLETE } from '../actions'

class Questionnaire extends Component {
    constructor(props) {
        super(props)    
        this.state = {
            questionIndex: 0
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.questionnaire.id !== this.props.questionnaire.id) {
            this.setState({questionIndex: 0})
        }
    }

    setAnswer(answer) {
        this.props.setAnswer(
            this.props.questionnaire.id,
            this.state.questionIndex,
            answer
        )
    }

    moveToQuestion(index, skip) {
        if (skip === true) {
            this.props.setAnswer(this.props.questionnaire.id, this.state.questionIndex, null);
        }

        if (index >= this.props.questionnaire.questions.length) {
            this.props.markComplete(this.props.questionnaire.id);
        } else {
            this.setState({questionIndex: index});
        }
    }

    renderQuestion(currentQuestion) {
        switch (currentQuestion.question.type) {
            case QuestionType.MULTIPLECHOICE:
                return <MultipleChoice options={currentQuestion.question.options} answer={currentQuestion.answer} setAnswer={this.setAnswer.bind(this)}/>
            case QuestionType.TEXT:
                return <Text answer={currentQuestion.answer} setAnswer={this.setAnswer.bind(this)}/>
            case QuestionType.RATING:
                return (
                    <React.Fragment>
                        <Typography.Text>{currentQuestion.question.description}</Typography.Text>
                        <Rating style={{marginTop: '10px'}} value={currentQuestion.answer} setValue={this.setAnswer.bind(this)}/>
                    </React.Fragment>
                )
        }
    }

    renderComplete() {
        return (
            <div>
                <h1>Thank you for sharing your feedback!</h1>
                <p>Continue to give feedback to other team members.</p>

                <QuestionnaireList questionnaires={this.props.incompleteQuestionnaires}/>
            </div>
        );
    }

    render() {
        const { questionnaire } = this.props;
        const currentQuestion = questionnaire.questions[this.state.questionIndex];
        const isLastQuestion = this.state.questionIndex === this.props.questionnaire.questions.length - 1;
        
        if (questionnaire.complete) {
            return this.renderComplete();
        }

        return (
            <div>
                <div><Link to="/share" className="uppercase-text"><Icon  style={{marginRight: '8px'}}type="left" />Back</Link></div>
                <div style={{display: 'flex', marginTop: '20px'}}>
                    <div>
                        <Typography.Title style={{fontSize: '31px', marginBottom: '4px'}} level={2}>{currentQuestion.question.text}</Typography.Title>
                        <Typography.Text className="uppercase-text">{`Share your feedback for ${questionnaire.reviewee.name}`}</Typography.Text>
                    </div>
                    <Avatar src={questionnaire.reviewee.avatar} style={{marginLeft: 'auto'}} size={58}/> 
                </div>
                <Card style={{marginTop: '20px'}}>
                    <div className="question">
                        {this.renderQuestion(currentQuestion)}
                    </div>
                    <div className="buttons" style={{display: 'flex', justifyContent: 'space-between', margin: '20px 0'}}>
                        <Button disabled={this.state.questionIndex === 0}style={{width: '150px'}} size="large" onClick={() => this.moveToQuestion(this.state.questionIndex-1)}>Previous</Button>
                        <Button style={{width: '150px'}} size="large" onClick={() => this.moveToQuestion(this.state.questionIndex+1, true)}>Skip</Button>
                        <Button 
                            type={isLastQuestion ? 'primary' : 'default'}
                            disabled={currentQuestion.answer == null}
                            style={{width: '150px'}} size="large" 
                            onClick={() => this.moveToQuestion(this.state.questionIndex+1)}
                            
                        >
                            {isLastQuestion ? 'Submit' : 'Next'}
                        </Button>
                    </div>
                    <Progress type="line" strokeColor={{from: 'rgb(38,219,173)', to: 'rgb(138,255,250)'}} percent={parseInt(100*this.state.questionIndex/questionnaire.questions.length)}/>
                </Card>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    let questionnaire = state.questionnaires[props.id];
    return {
        questionnaire: mapQuestionnaire(state, questionnaire),
        incompleteQuestionnaires: questionnairesForUser(state, state.app.currentUser).filter((q) => q.complete === false)
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setAnswer: (questionnaireId, questionIndex, answer) => dispatch({ type: QUESTION_ANSWER, questionnaireId, questionIndex, answer}),
        markComplete: (questionnaireId) => dispatch({type: QUESTIONNAIRE_COMPLETE, questionnaireId })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Questionnaire);
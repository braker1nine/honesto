import React, { Component } from 'react';
import { Select } from 'antd';

const { Option } = Select;

class PeriodSelector extends Component {

    render() {
        return (
            <div className="period-selector">
                <label>Feedback Period</label>
                <Select defaultValue="2019">
                    <Option value="2019">2019</Option>
                </Select>
            </div>
        )
    }
}

export default PeriodSelector;
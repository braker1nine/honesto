import React, { Component } from 'react';
import { Link } from "react-router-dom";

import { Divider, Button } from "antd";

class Login extends Component {
    render() {
        return (
            <div className="login">
                <div className="circle1"/>
                <div className="circle2"/>
                <div className="circle3"/>
                <div className="login-wrap">
                    <div className="login-card" >
                        <img alt="Logo" src="/images/Icon.svg"/>

                        <h1 style={{fontSize: '24px', fontWeight: '600'}}>Honesto</h1>
                        <Divider style={{width: '190px', minWidth: '190px'}}/>
                        <Link to="/share"><Button type="primary" style={{width: '190px'}}>Login with Google</Button></Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;
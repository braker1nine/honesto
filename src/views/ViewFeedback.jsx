import React from 'react';
import { connect } from "react-redux";
import { List, Avatar, Typography, Tag } from "antd";
import classnames from 'classnames';

import { questionnairesForUser } from "../selectors";
import { QuestionType } from "../constants";
import Rating from '../components/Rating';
import PeriodSelector from '../components/PeriodSelector';

class ViewFeedback extends React.Component {

    constructor(props) {
        super(props);
        
        let questionnaireIndex = 0;
        if (props.questionnaireId !== undefined) {
            let index = props.questionnaires.findIndex((q) => q.id === parseInt(props.questionnaireId));
            if (index > -1) {
                questionnaireIndex = index;
            }
        }
        this.state = {
            questionnaireIndex
        }
    }

    renderResponse(question) {
        let answer = null;
        let styles = {}

        switch (question.question.type) {
            case QuestionType.MULTIPLECHOICE:
                answer = (
                    <div className="answer" style={{marginTop: '10px'}}>
                        <span dangerouslySetInnerHTML={{__html: question.question.options[question.answer]}}/>
                    </div>
                );
                break;
            case QuestionType.TEXT:
                answer = <div className="answer" style={{marginTop: '10px'}}>{question.answer}</div>
                break;
            case QuestionType.RATING:
                styles = { display:'flex', alignItems: 'center', flexWrap: 'nowrap' };
                answer = <Rating value={question.answer} height={28} style={{flex: '50% 1 1', width: '100%', marginLeft: '10px'}}/>
                break;
        }

        let skippedTag = null
        if (question.answer === null) {
            skippedTag = <Tag color={"#ACB1B6"} style={{marginLeft: '8px'}}>Skipped</Tag>
            answer = null
        }

        return (
            <div style={{width: '100%', ...styles}}>
                <div className="question" style={{fontWeight: 500, color: question.answer === null ? '#ACB1B6' : '#031323', flex: '50% 1 1'}}>{question.question.text}{skippedTag}</div>
                {answer}
            </div>
        );
    }

    render() {
        return (
            <div className="view-feedback">
                <div className="container">
                    <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end'}}>
                        <h2 style={{marginBottom: 0}}>{this.props.title}</h2>
                        <PeriodSelector/>
                    </div>
                    <div className="list-wrapper" style={{minHeight: '100%', paddingTop: '20px'}}>
                        <List 
                            bordered
                            className="questionnaire-list"
                            style={{width: '35%', borderRight: 'none', borderTopRightRadius: 0, borderBottomRightRadius: 0}}
                            header={<span className="uppercase-text">Feedback {this.props.type === 'reviewee' ? 'Received' : 'Given'}</span>}
                        >
                            {this.props.questionnaires.map((questionnaire, index) => {
                                const user = this.props.type === 'reviewer' ? questionnaire.reviewee : questionnaire.reviewer;
                                return (
                                    <List.Item 
                                        onClick={() => this.setState({questionnaireIndex: index})}
                                        className={classnames({ active: this.state.questionnaireIndex === index})}
                                        key={questionnaire.id}
                                    >
                                        <Avatar src={user.avatar} size={58} style={{marginRight: '16px'}}/>
                                        <Typography.Text style={{fontSize: '16px', fontWeight: 500}}>{user.name}</Typography.Text>
                                    </List.Item>
                                );  
                            })}
                        </List>
                        <List bordered style={{width: '65%', borderTopLeftRadius: 0, borderBottomLeftRadius: 0}} className="question-list">
                            {this.props.questionnaires[this.state.questionnaireIndex].questions.map((question, index) => {
                                return (<List.Item key={index}>{this.renderResponse(question)}</List.Item>)
                            })}
                        </List>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state, props) => {
    const currentUserId = state.app.currentUser;
    return {
        questionnaires: questionnairesForUser(state, currentUserId, props.type).filter((q) => q.complete === true)
    }
}

export default connect(mapStateToProps)(ViewFeedback);
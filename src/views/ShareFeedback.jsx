import React from 'react';
import { connect } from 'react-redux';
import PeriodSelector from '../components/PeriodSelector';

import Questionnaire from '../components/Questionnaire';
import QuestionnaireList from '../components/QuestionnaireList';
import { currentUser, questionnairesForUser } from '../selectors';


class ShareFeedback extends React.Component {
    render() {
        const { match } = this.props;

        let content;
        if (match.params.questionnaireId) {
            content = <Questionnaire id={match.params.questionnaireId} history={this.props.history}/>
        } else {
            content = (
                <div>
                    <div className="share-feedback__header">
                        <h1>Share Feedback</h1>
                        <PeriodSelector />
                    </div>
                    <div className="share-feedback__content">
                        <QuestionnaireList questionnaires={this.props.questionnaires}/>
                    </div>
                </div>
            );
        }

        return (
            <div className="share-feedback">
                <div className="container">
                    {content}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    const user = currentUser(state);

    return {
        currentUser: user,
        questionnaires: questionnairesForUser(state, user.id)
    }
}

export default connect(mapStateToProps)(ShareFeedback);

export const currentUser = (state) => state.users[state.app.currentUser]
export const user = (state, id) => state.users[id]
export const questionForId = (state, id) => state.questions[id]

export const questionnairesForUser = (state, userId, type = 'reviewer') =>{
    const { questionnaires } = state;
    return Object.values(questionnaires).filter((q) => q[type] === userId).map((q) => mapQuestionnaire(state, q)).sort((a,b) => (a.complete === b.complete) ? 0 : a.complete ? 1 : -1);
}

// Convenience selector for mapping a questionnaire and filling it in with data
// Could definitely be a selector but I'm punting on reselect to favor
// development speed over efficiency
export const mapQuestionnaire = (state, q) => {
    return {
        reviewer: user(state, q.reviewer),
        reviewee: user(state, q.reviewee),
        id: q.id,
        complete: q.complete,
        questions: q.questions.map(question => ({ question: questionForId(state, question.questionId), answer: question.answer}))
    }
}
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider} from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Header from './components/Header'

import configureStore, { history } from './store';
import './styles/index.css';
import './styles/App.scss';

import * as serviceWorker from './serviceWorker';

import ViewFeedback from './views/ViewFeedback';
import ShareFeedback from './views/ShareFeedback';
import Login from './views/Login';

const store = configureStore()

class App extends React.Component {

    render() {
        return (
            <React.Fragment>
                <Header />
                <div style={{paddingTop: '72px'}}>
                    <Route path="/share/:questionnaireId?" component={ShareFeedback}/>
                    <Route path="/my/:questionnaireId?" render={({ match }) => {
                        return <ViewFeedback type="reviewee" title="My Feedback" questionnaireId={match.params.questionnaireId}/>
                    }}/>
                    <Route path="/team/:questionnaireId?" render={({ match }) => {
                        return <ViewFeedback type="reviewer" title="Team Feedback" questionnaireId={match.params.questionnaireId}/>
                    }}/>
                </div>
            </React.Fragment>
        );
    }
}

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Router>
                <Switch>
                    <Route path="/login" component={Login}/>
                    <Route exact path="/" render={() => (<Redirect to="/login"/>)}/>
                    <Route path="/" component={App} />
                </Switch>
            </Router>
        </ConnectedRouter>
    </Provider>, 
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


# Honesto
The app can be found at http://honesto.brakebill.me

## Things that weren't completed
All of the things that were not completed were planned to be left out in the original discussion. The only thing left out that I think maybe wasn't explicitly discussed was the empty state for viewing feedback. I chose to leave that out simply because you will never see it. The application is seeded with data that will prevent those feedback views from ever being empty

## Things I would have done differently with more time
- I would have had a much cleaner CSS setup. I ended up using CSS (well SCSS) somewhat sparingly in favor of inline styles. While inline styles are not particularly maintainable or extensible, they make for a really fast way to churn out UI. Especially when you don't have a ton of reuseable UI. Most of the UI that I reused was via components. Hooray React.
- Animations. I love animations and I probably would have used them more in the questionnaire view to move from question to question.
- Actual reselect selectors. I started thinking I would use them, but it was quicker to write a few functions for the features I needed and not worry about efficiency. If I had more time I'd take advantage of reselect's memoization.
- _Maybe_ added the empty states and loading states. Adding those would have made for a slightly more real feeling application. But given the data is seeded and all in memory, they aren't particularly necessary for this demo. If I had a few weeks of dedicated time I might have setup a simple node backend with a mongo database and then these views could have been more relevant.

## Other Details
- The app won't _perfectly_ match the designs but I think it's pretty darn close. I used the Ant Design library to speed up development and it gave me a nice set of UI components that I think were very close to the visual style of the design.
- As I said, I used a lot of inline CSS. I'm sorry for this and it's definitely not the way I'd usually style things but I was able to move very quickly that way. It definitely has me considering the benefits of CSS in JS.
- The app itself is a simple React, React-Router, Redux setup. Nothing super fancy, just a few basic routes and view components.